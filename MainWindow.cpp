#include <Python.h>
#include "MainWindow.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent) {
    setWindowTitle("C++ Qt Main Window");
    resize(800, 600);

    dockPanel = new DockablePanel("C++ Dockable Panel", this);
    addDockWidget(Qt::BottomDockWidgetArea, dockPanel);

    connect(dockPanel, &DockablePanel::showMessageRequested, this, &MainWindow::showMessage);

    setupPythonPanel();
}

MainWindow::~MainWindow() {
    Py_Finalize();
}

void MainWindow::showMessage() {
    QMessageBox messageBox;
    messageBox.setText("Hello, World!");
    messageBox.setWindowTitle("Message");
    messageBox.exec();
}

void MainWindow::setupPythonPanel() {
    Py_Initialize();

    intptr_t mainWindowPtr = reinterpret_cast<intptr_t>(this);
    std::string pointerStr = std::to_string(mainWindowPtr);

    std::string pythonInput = "import sys; sys.argv = ['Python_DockablePanel', '" + pointerStr + "']; sys.path.append(''); import Python_DockablePanel";
    PyRun_SimpleString(pythonInput.c_str());
}
