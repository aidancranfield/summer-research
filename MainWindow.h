#include <QMainWindow>
#include "DockablePanel.h"

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void showMessage();

private:
    void setupPythonPanel();
    DockablePanel* dockPanel;
};
