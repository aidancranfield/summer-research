#include "DockablePanel.h"

DockablePanel::DockablePanel(const QString& title, QWidget* parent)
    : QDockWidget(title, parent) {
    dockContentWidget = new QWidget(this);
    setWidget(dockContentWidget);

    dockLayout = new QVBoxLayout(dockContentWidget);
    label = new QLabel("This is a dockable panel made in C++.", dockContentWidget);
    dockLayout->addWidget(label);

    button = new QPushButton("Show Message", dockContentWidget);
    dockLayout->addWidget(button);

    connect(button, &QPushButton::clicked, this, &DockablePanel::showMessageRequested);
}
