from PySide6.QtWidgets import QDockWidget, QMainWindow, QWidget, QLabel, QVBoxLayout, QPushButton, QMessageBox
from PySide6.QtCore import Qt
from shiboken6 import wrapInstance
import sys


# Simple class for dockable panel with a lable, button, and message box
class DockablePanel(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("PySide6 Dockable Panel")
        layout = QVBoxLayout()

        # Add label
        layout.addWidget(QLabel("This is a dockable panel made in Python."))

        # Create a button
        self.button = QPushButton("Show Message")
        self.button.clicked.connect(self.showMessage)
        layout.addWidget(self.button)

        self.setLayout(layout)

    def showMessage(self):
        # Create a message box
        msgBox = QMessageBox()
        msgBox.setWindowTitle("Message")  
        msgBox.setText("Hello, World!")
        msgBox.exec_()

# Get the main window pointer from the arguments
mainWindowPtr = int(sys.argv[1])
'''
wrapInstance
Creates a Python wrapper for a C++ object instantiated at a given memory address 
- the returned object type will be the same given by the user.
(from https://doc.qt.io/qtforpython-6/shiboken6/shibokenmodule.html)
'''
mainWindow = wrapInstance(mainWindowPtr, QMainWindow)  

# Add python panel to main window
dockablePanel = DockablePanel()
dockWidget = QDockWidget("Python Dockable Panel", mainWindow)
dockWidget.setWidget(dockablePanel)
mainWindow.addDockWidget(Qt.TopDockWidgetArea, dockWidget)
dockablePanel.show()
