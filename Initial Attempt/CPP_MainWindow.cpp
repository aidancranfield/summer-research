#include <Python.h>
#include <QApplication>
#include <QMainWindow>
#include <QPushButton>
#include <QMessageBox>
#include <QDockWidget>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>



int main(int argc, char* argv[]) {

    // Setup application and main window
    QApplication app(argc, argv);
    QMainWindow mainWindow;
    mainWindow.setWindowTitle("C++ Qt Main Window");
    mainWindow.resize(800, 600);

    // Initialize Python Interpreter
    Py_Initialize();

    // Get main window's pointer to pass to python 
    intptr_t mainWindowPtr = reinterpret_cast<intptr_t>(&mainWindow);
    std::string pointerStr = std::to_string(mainWindowPtr);

    // Send pointer to python and load panel
    std::string pythonInput = "import sys; sys.argv = ['Python_DockablePanel', '" + pointerStr + "']; sys.path.append('\'); import Python_DockablePanel";
    PyRun_SimpleString(pythonInput.c_str());

    // Create a dockable panel in C++ with label and button
    QDockWidget dockWidget("C++ Dockable Panel", &mainWindow);
    mainWindow.addDockWidget(Qt::BottomDockWidgetArea, &dockWidget);
    QWidget dockContentWidget;
    dockWidget.setWidget(&dockContentWidget);
    QVBoxLayout dockLayout(&dockContentWidget);
    QLabel label("This is a dockable panel made in C++.", &dockContentWidget);
    dockLayout.addWidget(&label);

    // Create a button
    QPushButton button("Show Message", &dockContentWidget);
    dockLayout.addWidget(&button);

    // Connect the button's clicked signal to show a message box
    QObject::connect(&button, &QPushButton::clicked, [&]() {
        QMessageBox messageBox;
        messageBox.setText("Hello, World!");
        messageBox.setWindowTitle("Message");
        messageBox.exec();
        });

    mainWindow.show();
    app.exec();

    Py_Finalize();
}
