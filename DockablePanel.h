#include <QDockWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

class DockablePanel : public QDockWidget {
    Q_OBJECT

public:
    DockablePanel(const QString& title, QWidget* parent = nullptr);

signals:
    void showMessageRequested();

private:
    QWidget* dockContentWidget;
    QVBoxLayout* dockLayout;
    QLabel* label;
    QPushButton* button;
};
